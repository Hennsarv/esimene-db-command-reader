﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace ConsoleApp26
{
    class Program
    {
        static string connString = "Data Source=valiit.database.windows.net;Initial Catalog=northwind;User ID=student";
        static void Main(string[] args)
        {
            Console.Write("Parool: ");

            ConsoleKeyInfo k = new ConsoleKeyInfo();
            string password = "";
            do
            {
                k = Console.ReadKey(true);
                if (k.Key != ConsoleKey.Enter)
                {
                    password += k.KeyChar;
                    Console.Write("*");
                }

            } while (k.Key != ConsoleKey.Enter);

            SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder(connString);
            csb.Password = password;
            connString = csb.ConnectionString;

            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                using (SqlCommand comm = new SqlCommand("select companyname, contactname from customers", conn))
                {
                    var R = comm.ExecuteReader();
                    while (R.Read())
                    {
                        Console.WriteLine($"{R[0]} esindaja on {R[1]}");
                    }
                }
            }

        }
    }
}
